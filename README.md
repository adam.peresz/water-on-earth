## Water on Earth

## Description
Discover the ratio of the Earth's water bodies.

This simple and easy to use program helps you understand volume of the water on Earth and shows how small amount is accessible for humans.

At the startup the program generates 10,000,000 points, each point has their own color, the ratio of colors is the same as the different kind of waters on Earth. Using the "Next" and "Previous" buttons you can discover all the points. Use the "Mix" button to shuffle the points. With the color buttons it is possible to change the color of the points.

## Compatibility
This is a Python3 program using Python GTK+ for graphical interface.
The program is tested in Ubuntu 21.04. Before runnung the program you might need to install the python3-gi-cairo package.

$sudo apt-get install python3-gi-cairo

## Authors
Author: Adam Pereszlenyi

October 2021

## Support
You can contact me at adam@preparatorium.hu

## License
This is a free and open source program. Anyone can modify and spread it, but nobody can sell it for money.

## Project status
No longer developed.
