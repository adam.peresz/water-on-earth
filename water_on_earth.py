#!/bin/python3

#Author: Adam Pereszlenyi
#October 2021

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GLib, GdkPixbuf, GObject

import random
import time
import io
import cairo # $sudo aptitude install python3-gi-cairo
import numpy as np
import os

#Default colors:
ocean_color_default = [166/255.,196/255.,241/255.]
other_saline_color_default = [241/255.,166/255.,208/255.]
glaciers_color_default = [1,1,1]
groundwater_color_default = [213/255., 174/255., 137/255.]
ground_ice_and_permafrost_color_default = [189/255., 107/255., 11/255.]
lakes_color_default = [28/255.,113/255.,216/255.]
soil_moisture_color_default = [241/255.,255/255.,86/255.]
atmosphere_color_default = [175/255., 7/255., 218/255.]
swamp_color_default = [9/255.,104/255.,8/255.]
rivers_color_default = [13/255.,241/255.,44/255.]
living_things_color_default = [1,0,0]

class Window(Gtk.Window):
    def refresh_window(self):
        while Gtk.events_pending():
            Gtk.main_iteration()
        
    def about(self, button):
        dialog = Gtk.AboutDialog()
        dialog.set_title("About")
        dialog.set_program_name("Water on Earth")
        dialog.set_version("version 1.0\nOctober 2021")
        dialog.set_comments("Discover the ratio of the Earth's water bodies.")
        #TODO: Set website
        #dialog.set_website("http://skullbase.info")
        #dialog.set_website_label("Website")
        dialog.set_authors(["Adam Pereszlenyi\nadam@preparatorium.hu"])
        if os.path.isfile("./logo.png"):
            dialog.set_logo(GdkPixbuf.Pixbuf.new_from_file_at_size("./logo.png", 75, 75))
        dialog.set_license("This is a free and open source program.\nAnyone can modify and spread it, but nobody can sell it for money.\n\nData source: https://en.wikipedia.org/wiki/Water_distribution_on_Earth\n")
        dialog.connect('response', lambda dialog, data: dialog.destroy())
        dialog.show_all()

    def get_color(self, value):
        if value == 0:
            return self.ocean_color
        if value == 1:
            return self.other_saline_color
        if value == 2:
            return self.glaciers_color
        if value == 3:
            return self.groundwater_color
        if value == 4:
            return self.ground_ice_and_permafrost_color
        if value == 5:
            return self.lakes_color
        if value == 6:
            return self.soil_moisture_color
        if value == 7:
            return self.atmosphere_color
        if value == 8:
            return self.swamp_color
        if value == 9:
            return self.rivers_color
        if value == 10:
            return self.living_things_color

    def expose(self, widget=None, event=None):
        if self.page_counter == self.no_of_pages:
            self.progress_label.set_label('Congratulations! You saw all the {} points. Click on the "Mix" button and start to discover them again.'.format(f"{self.no_of_points:,}"))
        else:
            self.progress_label.set_label("Page {}/{} ({}%). You have already seen {} points.".format(self.page_counter, round(self.no_of_points/self.no_of_points_appeared), round((self.page_counter * 100) / self.no_of_pages, 2), f"{self.no_of_points_appeared * self.max_page_counter:,}"))

        array_to_show = np.reshape(self.numpy_array[:self.page_counter * self.no_of_points_appeared][-self.no_of_points_appeared:], (100, 200))

        self.progress_bar.set_fraction(self.page_counter / self.no_of_pages)

        self.cr = self.drawing_area.get_property('window').cairo_create()
        self.cr.set_line_width(1)
        self.cr.translate(0, 0)

        for i in range(len(array_to_show)):
            for j in range(len(array_to_show[0])):
                self.cr.set_source_rgb(*self.get_color(array_to_show[i][j]))
                self.cr.rectangle(1 + (j * 7), 1 + (i * 7), 6, 6)
                self.cr.fill()

        self.next_button.set_sensitive(False)
        self.previous_button.set_sensitive(False)
        self.refresh_window()
        time.sleep(0.5)
        if self.page_counter < self.no_of_pages:
            self.next_button.set_sensitive(True)
        if self.page_counter > 1:
            self.previous_button.set_sensitive(True)

    def color_chosen(self, user_data=None):
        self.ocean_color = [int(i) / 255. for i in self.ocean_color_button.get_rgba().to_string().replace('rgb(', '').replace(')', '').split(',')]
        self.other_saline_color = [int(i) / 255. for i in self.other_saline_color_button.get_rgba().to_string().replace('rgb(', '').replace(')', '').split(',')]
        self.glaciers_color = [int(i) / 255. for i in self.glaciers_color_button.get_rgba().to_string().replace('rgb(', '').replace(')', '').split(',')]
        self.groundwater_color = [int(i) / 255. for i in self.groundwater_color_button.get_rgba().to_string().replace('rgb(', '').replace(')', '').split(',')]
        self.ground_ice_and_permafrost_color = [int(i) / 255. for i in self.ground_ice_and_permafrost_color_button.get_rgba().to_string().replace('rgb(', '').replace(')', '').split(',')]
        self.lakes_color = [int(i) / 255. for i in self.lakes_color_button.get_rgba().to_string().replace('rgb(', '').replace(')', '').split(',')]
        self.soil_moisture_color = [int(i) / 255. for i in self.soil_moisture_color_button.get_rgba().to_string().replace('rgb(', '').replace(')', '').split(',')]
        self.atmosphere_color = [int(i) / 255. for i in self.atmosphere_color_button.get_rgba().to_string().replace('rgb(', '').replace(')', '').split(',')]
        self.swamp_color = [int(i) / 255. for i in self.swamp_color_button.get_rgba().to_string().replace('rgb(', '').replace(')', '').split(',')]
        self.rivers_color = [int(i) / 255. for i in self.rivers_color_button.get_rgba().to_string().replace('rgb(', '').replace(')', '').split(',')]
        self.living_things_color = [int(i) / 255. for i in self.living_things_color_button.get_rgba().to_string().replace('rgb(', '').replace(')', '').split(',')]

        self.expose()

    def previous_page(self, widget=None):
        self.page_counter -= 1
        self.expose()

    def next_page(self, widget=None):
        self.page_counter += 1
        if self.page_counter > self.max_page_counter:
            self.max_page_counter = self.page_counter
        self.expose()

    def on_key_press_event(self, widget, event):
        if event.keyval == 65363 and self.next_button.get_sensitive():
            self.next_page()
        if event.keyval == 65361 and self.previous_button.get_sensitive():
            self.previous_page()

    def mix(self, widget):
        self.page_counter = 1
        self.max_page_counter = 1
        self.previous_button.set_sensitive(False)
        self.next_button.set_sensitive(True)
        np.random.shuffle(self.numpy_array)
        self.expose()

    def default_colors(self, widget):
        self.ocean_color = ocean_color_default
        self.other_saline_color = other_saline_color_default
        self.glaciers_color = glaciers_color_default
        self.groundwater_color = groundwater_color_default
        self.ground_ice_and_permafrost_color = ground_ice_and_permafrost_color_default
        self.lakes_color = lakes_color_default
        self.soil_moisture_color = soil_moisture_color_default
        self.atmosphere_color = atmosphere_color_default
        self.swamp_color = swamp_color_default
        self.rivers_color = rivers_color_default
        self.living_things_color = living_things_color_default

        self.ocean_color_button.set_rgba(Gdk.RGBA(*self.ocean_color, 1.))
        self.other_saline_color_button.set_rgba(Gdk.RGBA(*self.other_saline_color, 1.))
        self.glaciers_color_button.set_rgba(Gdk.RGBA(*self.glaciers_color, 1.))
        self.groundwater_color_button.set_rgba(Gdk.RGBA(*self.groundwater_color, 1.))
        self.ground_ice_and_permafrost_color_button.set_rgba(Gdk.RGBA(*self.ground_ice_and_permafrost_color, 1.))
        self.lakes_color_button.set_rgba(Gdk.RGBA(*self.lakes_color, 1.))
        self.soil_moisture_color_button.set_rgba(Gdk.RGBA(*self.soil_moisture_color, 1.))
        self.atmosphere_color_button.set_rgba(Gdk.RGBA(*self.atmosphere_color, 1.))
        self.swamp_color_button.set_rgba(Gdk.RGBA(*self.swamp_color, 1.))
        self.rivers_color_button.set_rgba(Gdk.RGBA(*self.rivers_color, 1.))
        self.living_things_color_button.set_rgba(Gdk.RGBA(*self.living_things_color, 1.))

        self.expose()

    def quit_event(self, widget, event=None):
        Gtk.main_quit()
        
    def __init__(self):
        Gtk.Window.__init__(self, title="Water on Earth")
        self.set_position(Gtk.WindowPosition.CENTER)
        self.set_border_width(10)
        self.connect('delete-event', self.quit_event)
        self.connect("key-press-event", self.on_key_press_event)
        if os.path.isfile("./logo.png"):
            self.set_icon_from_file("./logo.png")

        #Setting parameters
        self.no_of_points = 10000000
        self.no_of_points_appeared = 20000
        self.no_of_pages = self.no_of_points / float(self.no_of_points_appeared)
        self.page_counter = 1
        self.max_page_counter = 1

        self.ocean_percentage = 0.965
        self.other_saline_percentage = 0.01
        self.freshwater_percentage = 0.025

        #Of freshwater
        self.glaciers_percentage = 0.687
        self.groundwater_percentage = 0.301
        self.other_freshwater_percentage = 0.012

        #Of other freshwater
        self.ground_ice_and_permafrost_percentage = 0.69
        self.lakes_percentage = 0.209
        self.soil_moisture_percentage = 0.038
        self.swamp_percentage = 0.026
        self.rivers_percentage = 0.0049
        self.living_things_percentage = 0.0026
        self.atmosphere_percentage = 0.03

        #Calculating no_of points
        self.other_saline_points = round(self.no_of_points * self.other_saline_percentage)
        self.freshwater_points = round(self.no_of_points * self.freshwater_percentage)

        self.other_freshwater_points = round(self.other_freshwater_percentage * self.freshwater_points)
        self.groundwater_points = round(self.groundwater_percentage * self.freshwater_points)
        self.glaciers_points = self.freshwater_points - (self.other_freshwater_points + self.groundwater_points)

        self.living_things_points = round(self.living_things_percentage * self.other_freshwater_points)
        self.rivers_points = round(self.rivers_percentage * self.other_freshwater_points)
        self.atmosphere_points = round(self.atmosphere_percentage * self.other_freshwater_points)
        self.swamp_points = round(self.swamp_percentage * self.other_freshwater_points)
        self.soil_moisture_points = round(self.soil_moisture_percentage * self.other_freshwater_points)
        self.lakes_points = round(self.lakes_percentage * self.other_freshwater_points)

        self.ground_ice_and_permafrost_points = self.other_freshwater_points - (self.living_things_points + self.rivers_points + self.atmosphere_points + self.swamp_points + self.soil_moisture_points + self.lakes_points)

        self.ocean_points = self.no_of_points - (self.other_saline_points + self.freshwater_points)

        self.ocean_color = ocean_color_default
        self.other_saline_color = other_saline_color_default
        self.glaciers_color = glaciers_color_default
        self.groundwater_color = groundwater_color_default
        self.ground_ice_and_permafrost_color = ground_ice_and_permafrost_color_default
        self.lakes_color = lakes_color_default
        self.soil_moisture_color = soil_moisture_color_default
        self.atmosphere_color = atmosphere_color_default
        self.swamp_color = swamp_color_default
        self.rivers_color = rivers_color_default
        self.living_things_color = living_things_color_default

        self.numpy_array = []
        for i in range(self.living_things_points):
            self.numpy_array.append(10)
        for i in range(self.rivers_points):
            self.numpy_array.append(9)
        for i in range(self.swamp_points):
            self.numpy_array.append(8)
        for i in range(self.atmosphere_points):
            self.numpy_array.append(7)
        for i in range(self.soil_moisture_points):
            self.numpy_array.append(6)
        for i in range(self.lakes_points):
            self.numpy_array.append(5)
        for i in range(self.ground_ice_and_permafrost_points):
            self.numpy_array.append(4)
        for i in range(self.groundwater_points):
            self.numpy_array.append(3)
        for i in range(self.glaciers_points):
            self.numpy_array.append(2)
        for i in range(self.other_saline_points):
            self.numpy_array.append(1)

        while len(self.numpy_array) < self.no_of_points:
            self.numpy_array.append(0)

        np.random.shuffle(self.numpy_array)

        self.box1 = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        self.add(self.box1)
        
        self.left_vertical_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.box1.add(self.left_vertical_box)

        label = Gtk.Label(label='We have {} points, these represent the water on Earth. Below {} points are shown. You can use the "Next" and "Previous" buttons to see all the points. Use the "Mix" button to shuffle the points.'.format(f"{self.no_of_points:,}", f"{self.no_of_points_appeared:,}"))
        self.left_vertical_box.add(label)

        self.points_frame = Gtk.Frame(label="{} points".format(f"{self.no_of_points_appeared:,}"))
        self.left_vertical_box.add(self.points_frame)

        self.drawing_area_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.points_frame.add(self.drawing_area_box)
        self.drawing_area_box.set_name("drawingarea")

        self.drawing_area = Gtk.DrawingArea()
        self.drawing_area_box.add(self.drawing_area)
        self.drawing_area.set_size_request(1400, 700)
        self.drawing_area.connect("draw", self.expose)
        col = Gdk.Color(0.7, 0.7, 0.7)
        css = '#drawingarea { background-color: #CFCFCF; }'
        css = str.encode(css)
        style_provider = Gtk.CssProvider()
        style_provider.load_from_data(css)

        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            style_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )

        self.progress_label = Gtk.Label()
        self.left_vertical_box.add(self.progress_label)

        self.progress_bar = Gtk.ProgressBar()
        self.left_vertical_box.add(self.progress_bar)

        self.right_vertical_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.box1.add(self.right_vertical_box)

        label = Gtk.Label(label="The points are colored as:")
        self.right_vertical_box.add(label)

        self.level1_table = Gtk.Table(n_rows=18, n_columns=4, homogeneous=False)
        self.right_vertical_box.add(self.level1_table)

        label = Gtk.Label(label="Percentage\n(%)")
        self.level1_table.attach(label, 1, 2, 0, 1, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label="No. of\npoints")
        self.level1_table.attach(label, 2, 3, 0, 1, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label="Color")
        self.level1_table.attach(label, 3, 4, 0, 1, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label="Ocean")
        self.level1_table.attach(label, 0, 1, 1, 2, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label=str(round(self.ocean_percentage * 100, 1)))
        self.level1_table.attach(label, 1, 2, 1, 2, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label=f"{self.ocean_points:,}")
        self.level1_table.attach(label, 2, 3, 1, 2, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        self.ocean_color_button = Gtk.ColorButton()
        self.ocean_color_button.connect("color-set", self.color_chosen)
        self.level1_table.attach(self.ocean_color_button, 3, 4, 1, 2, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)
        self.ocean_color_button.props.use_alpha = False
        self.ocean_color_button.set_rgba(Gdk.RGBA(*self.ocean_color))

        label = Gtk.Label(label="Other saline")
        self.level1_table.attach(label, 0, 1, 2, 3, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label=str(round(self.other_saline_percentage * 100, 1)))
        self.level1_table.attach(label, 1, 2, 2, 3, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label=f"{self.other_saline_points:,}")
        self.level1_table.attach(label, 2, 3, 2, 3, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        self.other_saline_color_button = Gtk.ColorButton()
        self.other_saline_color_button.connect("color-set", self.color_chosen)
        self.level1_table.attach(self.other_saline_color_button, 3, 4, 2, 3, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)
        self.other_saline_color_button.props.use_alpha = False
        self.other_saline_color_button.set_rgba(Gdk.RGBA(*self.other_saline_color))

        label = Gtk.Label(label="Freshwater")
        self.level1_table.attach(label, 0, 1, 3, 4, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label=str(round(self.freshwater_percentage * 100, 1)))
        self.level1_table.attach(label, 1, 2, 3, 4, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label=f"{self.freshwater_points:,}")
        self.level1_table.attach(label, 2, 3, 3, 4, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label="")
        self.level1_table.attach(label, 0, 1, 4, 5, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label="Freshwater divided to:")
        self.level1_table.attach(label, 0, 4, 5, 6, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label="Glaciers and\nice caps")
        self.level1_table.attach(label, 0, 1, 6, 7, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label=str(round(self.glaciers_percentage * 100, 1)))
        self.level1_table.attach(label, 1, 2, 6, 7, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label=f"{self.glaciers_points:,}")
        self.level1_table.attach(label, 2, 3, 6, 7, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        self.glaciers_color_button = Gtk.ColorButton()
        self.glaciers_color_button.connect("color-set", self.color_chosen)
        self.level1_table.attach(self.glaciers_color_button, 3, 4, 6, 7, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)
        self.glaciers_color_button.props.use_alpha = False
        self.glaciers_color_button.set_rgba(Gdk.RGBA(*self.glaciers_color))

        label = Gtk.Label(label="Groundwater")
        self.level1_table.attach(label, 0, 1, 7, 8, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label=str(round(self.groundwater_percentage * 100, 1)))
        self.level1_table.attach(label, 1, 2, 7, 8, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label=f"{self.groundwater_points:,}")
        self.level1_table.attach(label, 2, 3, 7, 8, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        self.groundwater_color_button = Gtk.ColorButton()
        self.groundwater_color_button.connect("color-set", self.color_chosen)
        self.level1_table.attach(self.groundwater_color_button, 3, 4, 7, 8, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)
        self.groundwater_color_button.props.use_alpha = False
        self.groundwater_color_button.set_rgba(Gdk.RGBA(*self.groundwater_color))

        label = Gtk.Label(label="Surface/other\nfreshwater")
        self.level1_table.attach(label, 0, 1, 8, 9, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label=str(round(self.other_freshwater_percentage * 100, 1)))
        self.level1_table.attach(label, 1, 2, 8, 9, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label=f"{self.other_freshwater_points:,}")
        self.level1_table.attach(label, 2, 3, 8, 9, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label="")
        self.level1_table.attach(label, 0, 4, 9, 10, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label="Surface/other freshwater divided to:")
        self.level1_table.attach(label, 0, 4, 10, 11, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label="Ground ice and\npermafrost")
        self.level1_table.attach(label, 0, 1, 11, 12, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label=str(round(self.ground_ice_and_permafrost_percentage * 100, 1)))
        self.level1_table.attach(label, 1, 2, 11, 12, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label=f"{self.ground_ice_and_permafrost_points:,}")
        self.level1_table.attach(label, 2, 3, 11, 12, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        self.ground_ice_and_permafrost_color_button = Gtk.ColorButton()
        self.ground_ice_and_permafrost_color_button.connect("color-set", self.color_chosen)
        self.level1_table.attach(self.ground_ice_and_permafrost_color_button, 3, 4, 11, 12, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)
        self.ground_ice_and_permafrost_color_button.props.use_alpha = False
        self.ground_ice_and_permafrost_color_button.set_rgba(Gdk.RGBA(*self.ground_ice_and_permafrost_color))

        label = Gtk.Label(label="Lakes")
        self.level1_table.attach(label, 0, 1, 12, 13, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label=str(round(self.lakes_percentage * 100, 1)))
        self.level1_table.attach(label, 1, 2, 12, 13, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label=f"{self.lakes_points:,}")
        self.level1_table.attach(label, 2, 3, 12, 13, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        self.lakes_color_button = Gtk.ColorButton()
        self.lakes_color_button.connect("color-set", self.color_chosen)
        self.level1_table.attach(self.lakes_color_button, 3, 4, 12, 13, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)
        self.lakes_color_button.props.use_alpha = False
        self.lakes_color_button.set_rgba(Gdk.RGBA(*self.lakes_color))

        label = Gtk.Label(label="Soil moisture")
        self.level1_table.attach(label, 0, 1, 13, 14, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label=str(round(self.soil_moisture_percentage * 100, 1)))
        self.level1_table.attach(label, 1, 2, 13, 14, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label=f"{self.soil_moisture_points:,}")
        self.level1_table.attach(label, 2, 3, 13, 14, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        self.soil_moisture_color_button = Gtk.ColorButton()
        self.soil_moisture_color_button.connect("color-set", self.color_chosen)
        self.level1_table.attach(self.soil_moisture_color_button, 3, 4, 13, 14, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)
        self.soil_moisture_color_button.props.use_alpha = False
        self.soil_moisture_color_button.set_rgba(Gdk.RGBA(*self.soil_moisture_color))

        label = Gtk.Label(label="Swamps, marshes")
        self.level1_table.attach(label, 0, 1, 14, 15, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label=str(round(self.swamp_percentage * 100, 1)))
        self.level1_table.attach(label, 1, 2, 14, 15, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label=f"{self.swamp_points:,}")
        self.level1_table.attach(label, 2, 3, 14, 15, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        self.swamp_color_button = Gtk.ColorButton()
        self.swamp_color_button.connect("color-set", self.color_chosen)
        self.level1_table.attach(self.swamp_color_button, 3, 4, 14, 15, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)
        self.swamp_color_button.props.use_alpha = False
        self.swamp_color_button.set_rgba(Gdk.RGBA(*self.swamp_color))

        label = Gtk.Label(label="Rivers")
        self.level1_table.attach(label, 0, 1, 15, 16, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label=str(round(self.rivers_percentage * 100, 2)))
        self.level1_table.attach(label, 1, 2, 15, 16, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label=f"{self.rivers_points:,}")
        self.level1_table.attach(label, 2, 3, 15, 16, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        self.rivers_color_button = Gtk.ColorButton()
        self.rivers_color_button.connect("color-set", self.color_chosen)
        self.level1_table.attach(self.rivers_color_button, 3, 4, 15, 16, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)
        self.rivers_color_button.props.use_alpha = False
        self.rivers_color_button.set_rgba(Gdk.RGBA(*self.rivers_color))

        label = Gtk.Label(label="Living things")
        self.level1_table.attach(label, 0, 1, 16, 17, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label=str(round(self.living_things_percentage * 100, 2)))
        self.level1_table.attach(label, 1, 2, 16, 17, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label=f"{self.living_things_points:,}")
        self.level1_table.attach(label, 2, 3, 16, 17, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        self.living_things_color_button = Gtk.ColorButton()
        self.living_things_color_button.connect("color-set", self.color_chosen)
        self.level1_table.attach(self.living_things_color_button, 3, 4, 16, 17, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)
        self.living_things_color_button.props.use_alpha = False
        self.living_things_color_button.set_rgba(Gdk.RGBA(*self.living_things_color))

        label = Gtk.Label(label="Atmosphere")
        self.level1_table.attach(label, 0, 1, 17, 18, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label=str(round(self.atmosphere_percentage * 100, 1)))
        self.level1_table.attach(label, 1, 2, 17, 18, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        label = Gtk.Label(label=f"{self.atmosphere_points:,}")
        self.level1_table.attach(label, 2, 3, 17, 18, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)

        self.atmosphere_color_button = Gtk.ColorButton()
        self.atmosphere_color_button.connect("color-set", self.color_chosen)
        self.level1_table.attach(self.atmosphere_color_button, 3, 4, 17, 18, Gtk.AttachOptions.EXPAND, Gtk.AttachOptions.EXPAND, 3, 3)
        self.atmosphere_color_button.props.use_alpha = False
        self.atmosphere_color_button.set_rgba(Gdk.RGBA(*self.atmosphere_color))

        self.button_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        self.right_vertical_box.add(self.button_box)

        self.previous_button = Gtk.Button(label="Previous")
        self.button_box.pack_start(self.previous_button, True, False, 3)
        self.previous_button.connect("clicked", self.previous_page)
        self.previous_button.set_sensitive(False)

        self.next_button = Gtk.Button(label="Next")
        self.button_box.pack_start(self.next_button, True, False, 3)
        self.next_button.connect("clicked", self.next_page)

        self.mix_button = Gtk.Button(label="Mix")
        self.button_box.pack_start(self.mix_button, True, False, 3)
        self.mix_button.connect("clicked", self.mix)

        self.default_colors_button = Gtk.Button(label="Default colors")
        self.button_box.pack_start(self.default_colors_button, True, False, 3)
        self.default_colors_button.connect("clicked", self.default_colors)

        self.about_button = Gtk.Button(label="About")
        self.button_box.pack_start(self.about_button, True, False, 3)
        self.about_button.connect("clicked", self.about)

        self.show_all()
        self.expose()
win = Window()
Gtk.main()
